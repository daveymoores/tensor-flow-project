import React, { Component, Fragment } from 'react';
import * as posenet from '@tensorflow-models/posenet';
import { VideoCanvasContainer, Button } from "./styles";

import { 
    isMobile
} from "../utils";

interface State {
    source?: string;
    imageScaleFactor: number;
    outputStride: any;
    flipHorizontal: boolean;
    videoHeight: number;
    videoWidth: number;
    desktop: boolean;
    localstream: any;
    net: any;
    selectedEmoji: string,
    loaded: boolean,
    netResponse: any,
    facingMode: string,
    emoji: Array<string>,
    devices: Array<string>
}

interface Props {
    poseLoaded: (loaded: boolean) => void;
}

interface Window {
    constraints: any;
    stream: any;
    addEventListener: (event: string, any) => void;
    removeEventListener: (event: string, any) => void;
    innerWidth: number;
    innerHeight: number;
}

interface Video {
    srcObject: object;
    play: any;
    onloadedmetadata: any;
}

interface Position {
    x: number;
    y: number;
}

interface Eyes {
    left?: Position;
    right?: Position;
}

interface Navigator {
    mediaDevices: any;
    getUserMedia(
        options: { video?: boolean; audio?: boolean; }, 
        success: (stream: any) => void, 
        error?: (error: string) => void
    ) : void;
}

declare const navigator: Navigator;
declare const window: Window;
declare const video: Video;
declare const eyes: Eyes;

const constraints = (window.constraints = {
    audio: false,
    video: true
});

export default class TensorCanvas extends Component<Props, State> {
    private videoRef: React.RefObject<HTMLVideoElement>;
    private canvasRef: React.RefObject<HTMLCanvasElement>;
    private video: any;
    private canvas: any;

    constructor(props: any) {
        super(props);
        this.videoRef = React.createRef();
        this.canvasRef = React.createRef();

        this.state = {
            imageScaleFactor: 0.5,
            outputStride: 16,
            flipHorizontal: false,
            videoWidth: window.innerWidth < 600 ? window.innerWidth : 600,
            videoHeight: window.innerWidth < 600 ? window.innerWidth * 0.85 : 500,
            localstream: null,
            desktop: true,
            net: null,
            loaded: false,
            netResponse: null,
            facingMode: 'user',
            selectedEmoji: "🌟",
            devices: [],
            emoji: ["✌", "😂", "😝", "😁", "😱", "👉", "🙌", "🍻", "🔥", "🌈", "☀", "🎈", "🌹", "💄", "🎀", "⚽", "🎾", "🏁", "😡", "👿", "🐻", "🐶", "🐬", "🐟", "🍀", "👀", "🚗", "🍎", "💝", "💙", "👌", "❤", "😍", "😉", "😓", "😳", "💪", "💩", "🍸", "🔑", "💖", "🌟", "🎉", "🌺", "🎶", "👠", "🏈", "⚾", "🏆", "👽", "💀", "🐵", "🐮", "🐩", "🐎", "💣", "👃", "👂", "🍓", "💘", "💜", "👊", "💋", "😘", "😜", "😵", "🙏", "👋", "🚽", "💃", "💎", "🚀", "🌙", "🎁", "⛄", "🌊", "⛵", "🏀", "🎱", "💰", "👶", "👸", "🐰", "🐷", "🐍", "🐫", "🔫", "👄", "🚲", "🍉", "💛", "💚"]
        };
    }

    componentDidMount() {
        this.video = this.videoRef.current;
        this.canvas = this.canvasRef.current;

        window.addEventListener("resize", this.handleResize);
        this.handleResize();
        this.switchEmoji();
        this.loadVideo();
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.handleResize);
    }

    handleResize = () => {
        this.setState({
            ...this.setMaxDimensions()
        })
    }

    setMaxDimensions = () => {
        const w = window.innerWidth;

        // should always be square
        const videoWidth = w < 600 ? w : 600;
        const videoHeight = w < 600 ? w * 0.95 : 500;
        const desktop = w > 600;

        return {
            videoWidth,
            videoHeight,
            desktop
        }
    }

    switchEmoji = () => {
        const { emoji } = this.state;

        this.setState({
            selectedEmoji: emoji[Math.floor(Math.random() * emoji.length) + 1 ]
        });
    }

    switchCamera = () => {
        const { facingMode, localstream } = this.state;
        const { poseLoaded } = this.props;
        const mode = (facingMode === "user") ? "environment" : "user";

        this.setState({ facingMode: mode, loaded: false }, 
        () => {
            poseLoaded(false); 
            localstream.getTracks()[0].stop();
            this.loadVideo();
        });
    }

    hasGetUserMedia() {
    return !!(navigator.mediaDevices &&
        navigator.mediaDevices.getUserMedia);
    }

    async loadVideo(){
        const video: any = await this.generateStream();
        video.play();
        this.loadPosenet();
    }

    async generateStream() {
        const { videoHeight, videoWidth, facingMode } = this.state;
        let devices;
        this.video.width = videoWidth;
        this.video.height = videoHeight;

        if (this.video && this.hasGetUserMedia()) {
            const mobile = isMobile();
            navigator.mediaDevices.enumerateDevices().then(gotDevices => {
                devices = gotDevices.reduce((acc, device)=>{
                    if(device.kind === "videoinput") {
                        acc.push(device.deviceId);
                    }
                    return acc;
                }, []);
                this.setState({devices});
            });
            const stream: string = await navigator.mediaDevices.getUserMedia({
                'audio': false,
                'video': {
                    facingMode,
                    width: mobile ? undefined : videoWidth,
                    height: mobile ? undefined : videoHeight,
                }
            });
            this.setState({localstream: stream});
            this.video.srcObject = stream;
        }

        return new Promise((resolve) => {
            this.video.onloadedmetadata = () => {
                resolve(this.video);
            };
        });
    };

    async loadPosenet() {
        const net = await posenet.load();
        this.setState({ net }, () => {
            this.detectEyePositionInRealTime();
        });
    }

    detectEyePositionInRealTime = () => {
        const {
            videoHeight,
            videoWidth,
            imageScaleFactor,
            outputStride,
            net,
            selectedEmoji,
            loaded,
            netResponse
        } = this.state;
        const { poseLoaded } = this.props;
        const ctx = this.canvas.getContext('2d');
        const flipHorizontal = true;
        const offset = 30;
        this.canvas.width = videoWidth;
        this.canvas.height = videoHeight;

        const eyes: Eyes = {};

        const generateImageData = async () => {

            const pose = await net.estimateSinglePose(
                this.video,
                imageScaleFactor,
                flipHorizontal,
                outputStride
            );

           if(!netResponse && !loaded) {
                this.setState({ netResponse: pose, loaded: true  }, () => {
                    poseLoaded(true); 
                });
            }

            this.animateCanvas(ctx, this.video, videoWidth, videoHeight, eyes, pose, offset);

            requestAnimationFrame(generateImageData);
        }

        generateImageData();
    };

    animateCanvas = (ctx, video, videoWidth, videoHeight, eyes, pose, offset) => {
        const { selectedEmoji } = this.state;

        ctx.save();
        ctx.scale(-1, 1);
        ctx.translate(-videoWidth, 0);
        ctx.drawImage(video, 0, 0, videoWidth, videoHeight);
        ctx.restore();

        eyes.left = pose.keypoints[1].position;
        eyes.right = pose.keypoints[2].position;

        ctx.font = '60px serif';

        ctx.fillText(selectedEmoji, (eyes.left.x - offset), (eyes.left.y + offset));
        ctx.fillText(selectedEmoji, (eyes.right.x - offset), (eyes.right.y + offset));
    }

    render() {
        const { source, videoHeight, videoWidth, devices } = this.state;
        return (
            <Fragment>
                <VideoCanvasContainer style={{width:`${videoWidth}px`, height: `${videoHeight}px`}}>
                    <video style={{width:`${videoWidth}px`, height: `${videoHeight}px`}} ref={this.videoRef} autoPlay />
                    <canvas style={{width:`${videoWidth}px`, height: `${videoHeight}px`}} ref={this.canvasRef} />
                </VideoCanvasContainer>
                <Button onClick={this.switchEmoji}>Switch Emoji</Button>
                { devices.length > 1 && <Button onClick={this.switchCamera}>Switch Camera</Button> }
            </Fragment>
        );
    }
}
