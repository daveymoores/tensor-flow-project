import React, { Fragment } from "react";
import { StyledCube, LoadingContainer, LoadingText } from "./styles";

const CanvasPreloader = ({ tensorLoading }) => {
    return (
        <Fragment>
            { !tensorLoading && 
            <LoadingContainer>
                <StyledCube
                color={'#1E1E1E'}
                bgColor={'#8DF2D2'}
                time={20000}
                />
                <LoadingText>Loading...</LoadingText>
            </LoadingContainer> }
        </Fragment>
    );
}

export default CanvasPreloader;