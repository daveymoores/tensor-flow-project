import styled from "styled-components";
import { Cube } from 'react-preloaders';

export const VideoCanvasContainer = styled.div`
    position: relative;
    display: block;
    margin: auto;
    overflow: hidden;
    margin-bottom: 60px;

    > video {
        position: absolute;
        width: 100%;
        top: 0;
        left: 0;
    }

    > canvas {
        position: absolute;
        width: 100%;
        top: 0;
        left: 0;
    }
`;

export const LoadingContainer = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    width: 100vw;
    height: 100%;
    height: 100vh;    
    z-index: 9; 
`;

export const StyledCube = styled(Cube)`
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate3d(-50%, -50%, 0);
    z-index: 10;
`;

export const Button = styled.button`
    font-family: 'Muli', sans-serif;
    font-weight: bold;
    border-radius: 5px;
    border: 2px solid #1E1E1E;
    background-color: transparent;
    box-sizing: border-box;
    text-aling: center;
    display: block;
    margin: 20px auto 0;
    cursor: pointer;
    font-size: 20px;
    padding: 12px 25px;
    outline: none;
`;

export const LoadingText = styled.p`
    text-align: center;
    display: block;
    margin: auto;
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate3d(-50%, -50%, 0);
    padding-top: 140px;
    color: #1E1E1E;
    z-index: 9999;
    font-size: 16px;
    font-weight: bold; 
`;