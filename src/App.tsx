import React, { Component } from 'react';
import styled from 'styled-components';
import TensorCanvas from './Components/TensorCanvas';
import CanvasPreloader from './Components/CanvasPreloader';
import { createGlobalStyle } from "styled-components";
import { reset } from 'styled-reset'

const GlobalStyle = createGlobalStyle`
  ${reset}

  body {
    background-color: #8DF2D2;
    font-family: 'Muli', sans-serif;
  }
`

interface State {
  loaded: boolean;
}


class App extends Component<{}, State> {
    state = {
      loaded: false
    }

    callback = (loaded) => {
      this.setState({ loaded });
    }

    render() {
        const { loaded } = this.state;
        return (
            <div className='App'>
                <GlobalStyle />
                <CanvasPreloader tensorLoading={loaded} />
                <TensorCanvas poseLoaded={this.callback} />
            </div>
        );
    }
}

export default App;
